FROM registry.gitlab.com/kimvanwyk/python3-flask-poetry:main
COPY ./svgifier/*.py /app/
COPY ./svgifier/templates/*.html /app/templates/
COPY ./svgifier/static/ /app/static/
ENV MODULE_NAME=app
ENV WEB_CONCURRENCY=2
