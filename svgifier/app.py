from dotenv import load_dotenv
from flask import Flask, render_template, request, send_file
from werkzeug.utils import secure_filename

import forms
import svg_api

import os

load_dotenv()

app = Flask(__name__)
app.config["SEND_FILE_MAX_AGE_DEFAULT"] = 0
app.config["SECRET_KEY"] = os.getenv("SECRET_KEY", "3wqr67ytrhko32ir23")

FILES_DIR = os.getenv("FILES_DIR", "/tmp")


# completely disable caching in the browser
@app.after_request
def add_header(response):
    # response.cache_control.no_store = True
    response.headers[
        "Cache-Control"
    ] = "private no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0"
    response.headers["Pragma"] = "no-cache"
    return response


@app.route("/", methods=["GET", "POST"])
def main():
    form = forms.InputForm()
    if form.validate_on_submit():
        f = form.file_name.data
        file_in = os.path.join(FILES_DIR, secure_filename(f.filename))
        print(f.content_length)
        f.save(file_in)

        p = svg_api.get_svg(file_in)
        return send_file(p, as_attachment=True, download_name=p.name)
    return render_template("input.html", form=form)
