from dotenv import load_dotenv
import httpx

import json
from pathlib import Path
import os

load_dotenv()

URL = os.getenv("SVG_URL", "http://127.0.0.1:4002/imagetracerjs/color")


def get_svg(fin):
    files = {"file": open(fin, "rb")}
    res = httpx.post(URL, files=files, timeout=60)
    d = json.loads(res.text)
    p = Path(fin).with_suffix(".svg")
    p.write_text(d["files"][0]["svg"])
    return p


if __name__ == "__main__":
    import sys

    get_svg(sys.argv[1])
