from flask_wtf import FlaskForm
from wtforms import SubmitField
from flask_wtf.file import FileField, FileRequired


class InputForm(FlaskForm):
    file_name = FileField(validators=[FileRequired()], description="Image to convert")
    submit = SubmitField("Convert image to SVG")
