// As per https://gist.github.com/marcus-at-localhost/a1092a4683730603fa8aeeee20d5b178

htmx.defineExtension('disable-submit', {
  onEvent: function (name, evt, data) {
    let elt = evt.detail.elt;
    let result = elt.querySelectorAll(".hx-disable");

    if (name === "htmx:beforeRequest") {
      result.forEach(element => element.disabled = true);
      if (elt.classList.contains("hx-disable")) elt.disabled = true;

    } else if(name == "htmx:afterRequest") {
      result.forEach(element => element.disabled = false);
      if (elt.classList.contains("hx-disable")) elt.disabled = false;
    }
  }
});
